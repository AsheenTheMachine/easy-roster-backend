# README #

.Net 6.0 Customer Backend API

### Features ###

* .Net 6.0
* C#
* ASP.Net Core
* Layered Architecture
* Exception Handling Middleware
* Visual Studio Code
* Microsoft SQL Server
* Entity Framework Core
* Concurreny Check applied on EF fields
* Linq Lambda Expressions
* SOLID Principles (Dependancy Injection, Single Resposibility, Interface Segregation)
* Respository Pattern + Generic Repository
* Asynchronous Tasks
* Unit of Work
* Swagger UI Documentation
* CRUD for Customer Domain


### How do I get set up? ###

#### The solution consists of the following projects ####

1.	App.API
2.	App.Domain
3.	App.Model
4.	App.Repository
5.	App.Test

#### Configuration ####

1.	Copy the clone link from this repo.
2.	Open Visual Studio Code or Visual Studio Professionl/Enterprise
3.	Click on clone repository and past the link. Alternatively, you may open Powershell and navigate to your projects folder. Then type `git clone CloneUrl` or right click, and your command pallete will automatically paste the link for you.

#### Dependencies
Visual Studio Code/Professional/Enterprise will automatically import dependecies required by the solution.

### API Endpoints
Browse `http://localhost:4000/swagger` for API endpoints and relevant information

### Who do I talk to? ###

* email asheenk@gmail.com for further assistance
