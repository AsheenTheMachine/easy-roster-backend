﻿namespace App.Domain.Helpers;

using System.ComponentModel.DataAnnotations;

public class BaseEntity : IBaseEntity
{
    [Key]
    public long Id { get; set; }

    public long GetId()
    {
        return Id;
    }
}
