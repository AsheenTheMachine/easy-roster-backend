﻿namespace App.Domain.Helpers;
public interface IBaseEntity
{
    long GetId();
}
