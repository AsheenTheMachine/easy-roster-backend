﻿namespace App.Domain;

using App.Domain.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public class Customer : BaseEntity
{
    //concurrency check to make sure 2 updates don't conflict
    [ConcurrencyCheck]
    public string FirstName { get; set; }

    [ConcurrencyCheck]
    public string Surname { get; set; }

    [ConcurrencyCheck]
    public string Email { get; set; }

    [ConcurrencyCheck]
    public string Cellphone { get; set; }

    [Column(TypeName = "money")]
    public decimal InvoiceTotal { get; set; }

    public int Type { get; set; }
}
