﻿namespace App.UI.Helpers;

using AutoMapper;
using App.Domain;
using App.Model;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<Customer, CustomerModel>().ReverseMap();
        CreateMap<Customer, CustomerModel>();
    }
}
