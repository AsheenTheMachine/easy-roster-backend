﻿namespace App.Api.Controllers;

using System.Threading.Tasks;
using App.Api.Helpers;
using App.Model;
using App.Repository.Helpers;
using App.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using App.Domain;
using AutoMapper;
using System.Net;

[Route("api/[controller]")]
[ApiController]
public class CustomerController : BaseController
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ICustomerRepository _customerRepository;

    public CustomerController(IUnitOfWork unitOfWork,
        IMapper mapper,
        ICustomerRepository customerRepository)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;

        _customerRepository = customerRepository;
    }


    [HttpGet]
    public IActionResult Index()
    {
        return Ok(HttpStatusCode.NotImplemented);
    }

    /// <summary>
    /// Create New Customer
    /// </summary>
    /// <param name="CustomerModel"></param>
    /// <returns></returns>
    [HttpPost("new")]
    public async Task<IActionResult> CreateCustomer([FromBody] CustomerModel model)
    {
        #region Validation
        if (!ModelState.IsValid)
            return BadRequest(new { message = GetErrors() });
        #endregion

        var customer = _mapper.Map<Customer>(model);

        // add customer object for inserting
        await _unitOfWork.Customer.Add(customer);
        int complete = await _unitOfWork.Complete();

        return Ok(complete);
    }

    /// <summary>
    /// Fetch Customer by ID
    /// </summary>
    /// <param name="id">id of customer object</param>
    /// <returns>CustomerModel</returns>
    [HttpGet("{id}")]
    public async Task<IActionResult> GetCustomerById(long id)
    {
        #region Validation
        if (id <= 0)
            return BadRequest(new { message = "Id must be greater than 0!" });
        #endregion

        //fetch customer by id
        var customer = await _unitOfWork.Customer.Get(id);
        await _unitOfWork.Complete();

        if (customer == null)
            return BadRequest(new { message = "Customer does not exist!" });

        var customerModel = _mapper.Map<CustomerModel>(customer);

        return Ok(customerModel);
    }


    /// <summary>
    /// Edit a Customer
    /// </summary>
    /// <param name="id">Id of the Customer to update</param>
    /// <param name="CustomerModel">Model of data to update</param>
    /// <returns></returns>
    [HttpPut("edit/{id}")]
    public async Task<IActionResult> UpdateCustomer([FromBody] CustomerModel model, long id)
    {
        #region Validation
        if (!ModelState.IsValid)
            return BadRequest(new { message = GetErrors() });
        if (id <= 0)
            return BadRequest(new { message = "Id must be greater than 0" });
        #endregion

        //fetch the customer for updating
        Customer customer = await _unitOfWork.Customer.Get(id);
        await _unitOfWork.Complete();

        //fill the customer domain with values from model
        customer.Cellphone = model.Cellphone;
        customer.Email = model.Email;
        customer.FirstName = model.FirstName;
        customer.InvoiceTotal = model.InvoiceTotal;
        customer.Surname = model.Surname;
        customer.Type = model.Type;

        // add customer object for updating
        _unitOfWork.Customer.Update(customer);
        return Ok(await _unitOfWork.Complete());
    }


    /// <summary>
    /// Delete a Customer
    /// </summary>
    /// <param name="Id">Customer Id</param>
    /// <returns></returns>
    [HttpDelete("remove/{id}")]
    public async Task<IActionResult> RemoveCustomer(long id)
    {
        #region Validation
        if (id <= 0)
            return BadRequest(new { message = "Id must be greater than 0" });
        #endregion

        var customer = await _unitOfWork.Customer.Get(id);
        _unitOfWork.Customer.Delete(customer);

        return Ok(await _unitOfWork.Complete());
    }

    /// <summary>
    /// Fetch Customer List
    /// </summary>
    /// <returns>A list of customers</returns>
    [HttpGet("list")]
    public async Task<IActionResult> GetAll()
    {
        var customerList = await _unitOfWork.Customer.GetAll();
        await _unitOfWork.Complete();
        return Ok(customerList);
    }
}
