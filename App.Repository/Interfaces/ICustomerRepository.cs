﻿namespace App.Repository.Interfaces;

using App.Domain;
using App.Repository.Helpers;

public interface ICustomerRepository : IGenericRepository<Customer>
{

}
