﻿namespace App.Repository.Helpers;

using App.Repository.Interfaces;

public interface IUnitOfWork
{

    ICustomerRepository Customer { get; }

    Task<int> Complete();

}
