﻿namespace App.Model;
using App.Model.Helpers;
using System.ComponentModel.DataAnnotations;

public class CustomerModel : BaseModel
{
    [Required(ErrorMessage = "First Name is required!")]
    [StringLength(50, ErrorMessage = "First Name cannot be longer than 50 characters!")]
    [Display(Name = "First Name")]
    public string FirstName { get; set; }

    [Required(ErrorMessage = "Surname is required!")]
    [StringLength(50, ErrorMessage = "Surname cannot be longer than 50 characters!")]
    public string Surname { get; set; }

    [Required(ErrorMessage = "Email is required!")]
    [StringLength(50, ErrorMessage = "Email cannot be longer than 100 characters!")]
    public string Email { get; set; }

    [Required(ErrorMessage = "Cellphone is required!")]
    [StringLength(13, ErrorMessage = "Cellphone cannot be longer than 13 characters!")]
    public string Cellphone { get; set; }

    public decimal InvoiceTotal { get; set; }

    [Required(ErrorMessage = "Customer Type is required!")]
    [Display(Name = "Customer Type")]
    public int Type { get; set; }
}

