﻿namespace App.Model.Helpers;

using System;

public class BaseModel : IBaseModel
{
    public long Id { get; set; }

    public long GetId()
    {
        return Id;
    }
}